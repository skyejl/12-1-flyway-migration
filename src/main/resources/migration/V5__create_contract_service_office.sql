CREATE TABLE `contract_service_office` (
  `officeId` int NOT NULL,
  `contractId` int NOT NULL,
   PRIMARY KEY(`officeId`,`contractId`),
  FOREIGN KEY (`officeId`) REFERENCES `office`(`id`),
  FOREIGN KEY(`contractId`) REFERENCES `contract`(`id`)
)
