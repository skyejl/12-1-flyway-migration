CREATE TABLE `branch` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `clientid` int NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY(`clientid`) REFERENCES client(`id`)
);
