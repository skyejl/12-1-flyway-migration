CREATE TABLE `staff` (
  `id` int NOT NULL AUTO_INCREMENT,
  `officeId` int NOT NULL,
  `firstName` varchar(45) NOT NULL,
  `lastName` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`officeId`) REFERENCES `office` (`id`)
)
