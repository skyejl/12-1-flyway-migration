CREATE TABLE `contract` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `branchid` int NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`branchid`) REFERENCES `branch` (`id`)
);


