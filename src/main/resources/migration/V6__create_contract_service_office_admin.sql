ALTER TABLE `contract_service_office`
ADD `staffId` int NOT NULL;
ALTER TABLE `contract_service_office`
ADD FOREIGN KEY (`staffId`) REFERENCES `staff` (`id`);
